var pickedColor;
var colors = [];
var numSquares = 6;
var squares = document.querySelectorAll(".square");
var colorDisplay = document.querySelector("h1 span");
var messageDisplay = document.querySelector("#message");
var h1 = document.querySelector("h1");
var resetButton = document.querySelector("#reset-button");
var modeButtons = document.querySelectorAll(".mode");

init();
resetGame();

function init() {
	setupButtons();
	setupSquares();
	resetGame();
}

function resetGame() {
	colors = generateRandomColorsArray(numSquares);
	pickedColor = pickColor();
	colorDisplay.textContent = pickedColor;
	updateSquareColors();
	h1.style.backgroundColor = "steelblue";
	messageDisplay.textContent = "";
	resetButton.textContent = "New Colors";
}

function setupButtons() {
	// Setup Mode Buttons
	for(let i = 0; i < modeButtons.length; i++) {
		modeButtons[i].addEventListener("click", function() {
			modeButtons[0].classList.remove("selected");
			modeButtons[1].classList.remove("selected");
			this.classList.add("selected");
			this.textContent === "EASY" ? numSquares = 3 : numSquares = 6;
			resetGame();
		});
	}

	// Setup reset button 
	resetButton.addEventListener("click", function() {
		resetGame();
	});
}

function setupSquares() {
	for(var i = 0; i < squares.length; i++) {
		squares[i].addEventListener("click", function(){
			var clickedColor = this.style.backgroundColor;
			if(clickedColor === pickedColor) {
				messageDisplay.textContent = "Correct!";
				changeColors(clickedColor);
				h1.style.backgroundColor = clickedColor;
				resetButton.textContent = "Play Again?";
			} else {
				this.style.backgroundColor = "#232323";
				messageDisplay.textContent = "Try Again";
			}
		});
	}
}

function updateSquareColors() {
	for(var i = 0; i < squares.length; i++) {
		if(colors[i]) {
			squares[i].style.display = "block";
			squares[i].style.backgroundColor = colors[i];
		} else {
				squares[i].style.display = "none";
		}
	}
}

function changeColors(color) {
	for (var i = 0; i < squares.length; i++) {
		squares[i].style.backgroundColor = color;
	}
}

// Returns a random color from the colors array 
function pickColor() {
	var random = Math.floor(Math.random() * colors.length); //Rand number from 0 to 5
	return colors[random];
}

function generateRandomColorsArray(length) {
	var arr = [];
	for (var i = 0; i < length; i++) {
		arr.push(randomColor());
	}
	return arr;
}

function randomColor() {
	var r = Math.floor(Math.random() * 256);
	var g = Math.floor(Math.random() * 256);
	var b = Math.floor(Math.random() * 256);
	return "rgb(" + r + ", " + g + ", " + b + ")";
}



